﻿#include <iostream>
#include <algorithm>

using namespace std;

char gridView[100][100];
int dataGrid[100][100];
int countColumn = 9;
int countRow = 9;
int countMine = 10;

void initArrayDefVal() {
    for (int i = 0; i < countColumn; ++i) {
        for (int j = 0; j < countRow; ++j) {
            gridView[i][j] = '#';
            dataGrid[i][j] = 0;
        }
    }
}

void viewPlayingField() {
    cout << "   ";
    for (int i = 0; i < countColumn; ++i) {
        cout << i + 1 << ' ';
    }
    cout << endl;
    for (int i = 0; i < countColumn; ++i) {
        if (i < 9) {
            cout << i + 1 << "  ";
        } else {
            cout << i + 1 << " ";
        }
        for (int j = 0; j < countRow; ++j) {
            cout << gridView[i][j] << ' ';
        }
        cout << endl;
    }
    cout << endl;
}

void viewDataField() {
    cout << "   ";
    for (int i = 0; i < countColumn; ++i) {
        cout << i + 1 << ' ';
    }
    cout << endl;
    for (int i = 0; i < countColumn; ++i) {
        if (i < 9) {
            cout << i + 1 << "  ";
        } else {
            cout << i + 1 << " ";
        }
        for (int j = 0; j < countRow; ++j) {
            if (dataGrid[i][j] == -1) {
                cout << "* ";
            } else {
                cout << dataGrid[i][j] << ' ';
            }
        }
        cout << endl;
    }
}

void initAroundMine(int x, int y) {
    for (int i = x - 1; i <= x + 1; ++i) {
        for (int j = y - 1; j <= y + 1; ++j) {
            if (i >= 0 && j >= 0 && !(i == x && j == y) && dataGrid[i][j] != -1) {
                dataGrid[i][j]++;
            }
        }
    }
}

void initDataGrid() {
    cout << endl;
    srand(time(0));
    for (int i = 0; i < countMine; ++i) {
        int x, y;
        do {
            x = rand() % countColumn;
            y = rand() % countRow;
        } while (dataGrid[x][y] == -1);
        dataGrid[x][y] = -1;
        initAroundMine(x, y);
    }
}

char getCharForCoordinate(int i, int j) {
    switch (dataGrid[i][j]) {
        case 1: {
            return '1';
            break;
        }
        case 2: {
            return '2';
            break;
        }
        case 3: {
            return '3';
            break;
        }
        case 4: {
            return '4';
            break;
        }
        case 5: {
            return '5';
            break;
        }
        case 6: {
            return '6';
            break;
        }
    }
}

void openVoidCell(int x, int y) {
    for (int i = x - 1; i <= x + 1; ++i) {
        for (int j = y - 1; j <= y + 1; ++j) {
            if (i >= 0 && j >= 0 && !(i == x && j == y)) {
                if (dataGrid[i][j] == 0 && gridView[i][j] == '#') {
                    gridView[i][j] = ' ';
                    openVoidCell(i, j);
                } else if (dataGrid[i][j] > 0 && gridView[i][j] == '#') {
                    gridView[i][j] = getCharForCoordinate(i, j);
                }
            }
        }
    }
}

int completeCommand(char comand) {
    if (comand == 'o') {
        cout << "Enter coordinate 'x' and 'y' through a space: ";
        int x, y;
        while (true) {
            cin >> x >> y;
            x--;
            y--;
            if (x < 0 || x > countColumn || y < 0 || y > countRow) {
                cout << "Invalid coordinate! Try again!";
            } else {
                if (dataGrid[x][y] == -1) {
                    cout << "Your lose!\n";
                    return -1;
                } else if (dataGrid[x][y] > 0) {
                    gridView[x][y] = getCharForCoordinate(x, y);
                } else if (dataGrid[x][y] == 0) {
                    openVoidCell(x, y);
                }
                break;
            }
        }
    } else if (comand == 'f') {
        cout << "Enter coordinate 'x' and 'y' through a space: ";
        int x, y;
        while (true) {
            cin >> x >> y;
            x--;
            y--;
            if (x < 0 || x > countColumn || y < 0 || y > countRow) {
                cout << "Invalid coordinate! Try again!";
            } else {
                gridView[x][y] = 'F';
                break;
            }
        }
    }
    return 0;
}

int enterCommand() {
    cout << endl;
    cout << "list of available commands:\n";
    cout << "1) o - open cell.\n";
    cout << "2) f - set flag mine.\n";
    cout << "3) n - run new game.\n";
    cout << "4) c - close app.\n";
    cout << "Enter command:\n";
    char comand;
    while (true) {
        cin >> comand;
        if (comand == 'o' || comand == 'f' || comand == 'n' || comand == 'c') {
            if (completeCommand(comand) == -1) return -1;
            break;
        } else cout << "Invalid command entered, try enter again!\n";
    }
    return 0;
}

void gamePlay() {

    while (true) {
        system("cls");
        viewPlayingField();
//        viewDataField();
        if (enterCommand() == -1) break;
        int countOpenCell = 0;
        int countFindMine = 0;
        for (int i = 0; i < countColumn; ++i) {
            for (int j = 0; j < countRow; ++j) {
                if (gridView[i][j] != '#' && gridView[i][j] != 'F') countOpenCell++;
                if (gridView[i][j] == 'F' && dataGrid[i][j] == -1) countFindMine++;
            }
        }
        if (countOpenCell == countColumn * countRow - countMine) {
            if (countFindMine == countMine) {
                cout << "You Win! Congratulations!\n";
                break;
            }
        }
    }
    cout << "enter any string and push Enter for close app";
    string str;
    cin >> str;
}

int main() {
    initArrayDefVal();
    initDataGrid();
    gamePlay();
}


